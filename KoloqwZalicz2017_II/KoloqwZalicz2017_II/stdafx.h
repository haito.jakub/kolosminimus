// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>



// TODO: reference additional headers your program requires here

struct MY_POINT
{
	int *crd; //wspolrzedne w pikselach punkta na plaszczyznie: crd[0] - x, crd[1] - y
};

struct MY_QUADRILAT
{
	MY_POINT vert[4];  //to jest czworokat, vert - jego wierzcholki
};
