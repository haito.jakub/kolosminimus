﻿// KoloqwZalicz2017_II.cpp : Defines the entry point for the console application.
// MY_QUADRILAT - to jest czworokat, skladajacy sie z 4 wierzcholkow typu MY_POINT
// MY_POINT - typ wierzcholku na plaszczyznie, zawiera dwie wspolrzedne, 
// umieszczone w tablice crd alokowana dynamicznie (patrz stdafx.h)

#include "stdafx.h"
#include "stdlib.h"

#pragma warning ( disable : 4996 )

//Obsluga MY_POINT

//Tworzy dynamicznie obiekt MY_POINT ze wspolrzednymi x, y i zwraca do niego wskaznik
MY_POINT * MY_POINT_Init(int x, int y);

//Zwalnia pamiec zajmowana obiektem MY_POINT
void MY_POINT_Free(MY_POINT *pt);

//Wyprowadza na monitor wspolrzedne obiektu MY_POINT; str - wirsz tekstowy zawierajacy numer tego wierzcholku
void MY_POINT_Print(MY_POINT *pt, char *str);

//Przypisuje wspolrzedne obiektu ptp do ptl (poprawnie wykonuje *ptl = *ptp) 
void MY_POINT_Equel(MY_POINT *ptl, MY_POINT *ptp);

//Alokuje pamiec dla tablicy crd obiektu MY_POINT, tworzonego statycznie.
int MY_POINT_Alloc_crd(MY_POINT *pt);

//Zwalnia pamiec dla tablicy crd obiektu MY_POINT, tworzonego statycznie.
void MY_POINT_Free_crd(MY_POINT *pt);

//Obsluga MY_QUADRILAT

//tworzy dynamicznie obiekt MY_QUADRILAT i zwraca do niego wskaznik. 
//tab_vert - tablica z czterech elementow typu MY_POINT
MY_QUADRILAT * MY_QUADRILAT_Init(MY_POINT *tab_vert);

//Zwalnia pamiec zajmowana obiektem MY_QUADRILAT
void MY_QUADRILAT_Free(MY_QUADRILAT *pq);

//===========================================================================
//Napisz obsluge MY_POINT

//===========================================================================
//Obsluga MY_QUADRILAT

MY_POINT * MY_POINT_Init(int x, int y)
{
	MY_POINT *point = (MY_POINT *)malloc(sizeof(MY_POINT));

	if (point) {
		if (MY_POINT_Alloc_crd(point)) {
			point->crd[0] = x;
			point->crd[1] = y;
		}
	}

	return point;
}

void MY_POINT_Free(MY_POINT * pt)
{
	if (pt) {
		MY_POINT_Free_crd(pt);
		free(pt);
	}
}

void MY_POINT_Print(MY_POINT * pt, char * str)
{
	printf("Vertex numer: %s\n", str);
	printf("x: %d, y: %d \n", pt->crd[0], pt->crd[1]);
}

void MY_POINT_Equel(MY_POINT * ptl, MY_POINT * ptp)
{
	ptl->crd[0] = ptp->crd[0];
	ptl->crd[1] = ptp->crd[1];
}

int MY_POINT_Alloc_crd(MY_POINT * pt)
{
	pt->crd = (int*)malloc(sizeof(int) * 2);
	return pt->crd ? 1 : 0;
}

void MY_POINT_Free_crd(MY_POINT * pt)
{
	if (pt->crd) {
		free(pt->crd);
	}
}

MY_QUADRILAT * MY_QUADRILAT_Init(MY_POINT *tab_vert)
{
	MY_QUADRILAT *pq = (MY_QUADRILAT *)malloc(sizeof(MY_QUADRILAT));
	
	if (pq)
	{
		for (int i = 0; i < 4; ++i)
		{
			pq->vert[i].crd = NULL;
			MY_POINT_Alloc_crd(&pq->vert[i]);
			MY_POINT_Equel(&pq->vert[i], &tab_vert[i]);
			//pq->vert[i] = tab_vert[i];
		}
	}

	return pq;
}

void MY_QUADRILAT_Free(MY_QUADRILAT *pq)
{
	if (pq)
	{
		for (int i = 0; i < 4; ++i)
			MY_POINT_Free_crd(&pq->vert[i]);
		free(pq);
	}
}

void MY_QUADRILAT_Print(MY_QUADRILAT *pq)
{
	char str[128];

	if (pq)
	{
		printf("Quadrangle\n");

		for (int i = 0; i < 4; ++i)
		{
			sprintf(str, "%d", i);
			MY_POINT_Print(&pq->vert[i], str);
		}
	}
}

//===============================================================
int main()
{
	MY_POINT * tab[4], tab_v[4];
	int x[] = {0, 10, 11, 2};
	int y[] = {0, -1, 6, 5};

	memset(tab, 0, sizeof(tab));
	memset(tab_v, 0, sizeof(tab_v));
	
	for (int i = 0; i < 4; ++i)
	{
		MY_POINT_Alloc_crd(&tab_v[i]);
		tab[i] = MY_POINT_Init(x[i], y[i]);
		MY_POINT_Equel(&tab_v[i], tab[i]);
		//tab_v[i] = *(tab[i]);
	}

	MY_QUADRILAT * quad = MY_QUADRILAT_Init(tab_v);
	MY_QUADRILAT_Print(quad);
	MY_QUADRILAT_Free(quad);

	for (int i = 0; i < 4; ++i)
	{
		MY_POINT_Free(tab[i]);
		tab[i] = NULL;
		MY_POINT_Free_crd(&tab_v[i]);
	}

	system("pause");
    return 0;
}

